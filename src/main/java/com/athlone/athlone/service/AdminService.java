package com.athlone.athlone.service;

import com.athlone.athlone.entry.Admin;
import org.springframework.stereotype.Service;

@Service
public interface AdminService {

    public Admin login(String userName, String password);
}
