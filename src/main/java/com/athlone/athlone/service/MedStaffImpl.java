package com.athlone.athlone.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;
import com.athlone.athlone.entry.MedStaff;
import com.athlone.athlone.mapper.MedStaffMapper;

import java.util.List;

@Service
@Transactional
public class MedStaffImpl implements MedStaffService{

    @Autowired
    private MedStaffMapper medStaffMapper;

    @Override
    public MedStaff insert(String userName, String password) {
        MedStaff tmp = new MedStaff();
        tmp.setUserName(userName);
        tmp.setPassWord(password);
        medStaffMapper.insertMedStaff(tmp);
        return null;
    }

    @Override
    public Boolean checkIfExist(String userName) {
        return medStaffMapper.selectMedStaff(userName);
    }

    @Override
    public MedStaff login(String userName, String password) {
        Example example = new Example(MedStaff.class);
        example.createCriteria().andEqualTo("userName",userName).andEqualTo("passWord",password);
        List<MedStaff> staffs = medStaffMapper.selectByExample(example);
        if(!CollectionUtils.isEmpty(staffs)){
            return staffs.get(0);
        }
        return null;
    }
}
