package com.athlone.athlone.service;

import com.athlone.athlone.entry.Patient;

import java.util.List;

public interface PatientService {
    public Patient login(String username, String password);
    public List<Patient> selectAll();
    public void insert(Patient tmp);
    public void update(Patient tmp);
    public Boolean checkIfExist(String userName);
}
