package com.athlone.athlone.service;

import com.athlone.athlone.entry.MedStaff;
import com.athlone.athlone.entry.Pharmacist;
import org.springframework.stereotype.Service;

@Service
public interface MedStaffService {
    public MedStaff insert(String userName, String password);
    public Boolean checkIfExist(String userName);
    public MedStaff login(String userName, String password);
}
