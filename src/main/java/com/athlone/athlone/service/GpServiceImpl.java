package com.athlone.athlone.service;

import com.athlone.athlone.common.Result;
import com.athlone.athlone.entry.Gp;
import com.athlone.athlone.mapper.GpMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
@Service
@Transactional
public class GpServiceImpl implements  GpService {

    @Autowired
    private GpMapper gpMapper;
    
    @Override
    public Gp login(String userName, String password) {
        Example example = new Example(Gp.class);
        example.createCriteria().andEqualTo("userName",userName).andEqualTo("passWord",password);
        List<Gp> gps = gpMapper.selectByExample(example);
        if(!CollectionUtils.isEmpty(gps)){
            return gps.get(0);
        }
        return null;
    }
    
    public Gp add(String userName, String password) {
    	Gp gp = new Gp();
    	gp.setUserName(userName);
    	gp.setPassWord(password);
    	gpMapper.insertSelective(gp);
    	return gp;
    	
    };

    @Override
    public Boolean checkIfExist(String userName) {
        Example example = new Example(Gp.class);
        example.createCriteria().andEqualTo("userName", userName);
        List<Gp> gps = gpMapper.selectByExample(example);
        return !CollectionUtils.isEmpty(gps);
    }
}