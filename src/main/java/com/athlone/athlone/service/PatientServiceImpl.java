package com.athlone.athlone.service;

import com.athlone.athlone.entry.Gp;
import com.athlone.athlone.entry.Patient;
import com.athlone.athlone.mapper.PatientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
@Transactional
public class PatientServiceImpl implements PatientService{

    @Autowired
    private PatientMapper patientMapper;

    @Override
    public Patient login(String username, String password) {
        Example example = new Example(Patient.class);
        example.createCriteria().andEqualTo("userName",username).andEqualTo("passWord",password);
        List<Patient> patients = patientMapper.selectByExample(example);
        if(!CollectionUtils.isEmpty(patients)){
            return patients.get(0);
        }
        return null;
    }

    @Override
    public List<Patient> selectAll() {
        return patientMapper.selectAllPatients();
    }

    @Override
    public void insert(Patient tmp) {
        patientMapper.insertPatient(tmp);
    }

    @Override
    public void update(Patient tmp) {
        patientMapper.updatePatient(tmp);
    }

    @Override
    public Boolean checkIfExist(String userName) {
        Example example = new Example(Patient.class);
        example.createCriteria().andEqualTo("userName", userName);
        List<Patient> patients = patientMapper.selectByExample(example);
        return !CollectionUtils.isEmpty(patients);
    }
}
