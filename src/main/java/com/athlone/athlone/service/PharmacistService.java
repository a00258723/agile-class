package com.athlone.athlone.service;

import com.athlone.athlone.entry.Pharmacist;

public interface PharmacistService {
    public Pharmacist login(String userName, String password);
    public Pharmacist insert(String userName, String password);
    public Boolean checkIfExist(String userName);
}
