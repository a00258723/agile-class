package com.athlone.athlone.service;

import com.athlone.athlone.entry.Pharmacist;
import com.athlone.athlone.mapper.PharmacistMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
@Transactional
public class PharmacistServiceImpl implements  PharmacistService {

    @Autowired
    private PharmacistMapper pharmacistMapper;

    @Override
    public Pharmacist insert(String userName, String password) {
        Pharmacist tmp = new Pharmacist();
        tmp.setUserName(userName);
        tmp.setPassWord(password);
        pharmacistMapper.insertPharmacist(tmp);
        return null;
    }

    @Override
    public Boolean checkIfExist(String userName) {
        return pharmacistMapper.selectPharmacist(userName);
    }

    @Override
    public Pharmacist login(String userName, String password) {
        Example example = new Example(Pharmacist.class);
        example.createCriteria().andEqualTo("userName",userName).andEqualTo("passWord",password);
        List<Pharmacist> pharmacists = pharmacistMapper.selectByExample(example);
        if(!CollectionUtils.isEmpty(pharmacists)){
            return pharmacists.get(0);
        }
        return null;
    }

}
