package com.athlone.athlone.service;

import com.athlone.athlone.entry.Admin;
import com.athlone.athlone.mapper.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
@Service
@Transactional
public class AdminServiceImpl implements  AdminService {

    @Autowired
    private AdminMapper adminMapper;

    @Override
    public Admin login(String userName, String password) {
        Example example = new Example(Admin.class);
        example.createCriteria().andEqualTo("userName",userName).andEqualTo("passWord",password);
        List<Admin> admins = adminMapper.selectByExample(example);
        if(!CollectionUtils.isEmpty(admins)){
            return admins.get(0);
        }
        return null;
    }

}
