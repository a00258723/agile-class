package com.athlone.athlone.service;

import com.athlone.athlone.entry.Gp;
import org.springframework.stereotype.Service;

@Service
public interface GpService {

    public Gp login(String userName, String password);
    public Gp add(String userName, String password);
    public Boolean checkIfExist(String userName);

}
