package com.athlone.athlone.common;

import com.athlone.athlone.entry.Gp;
import com.athlone.athlone.mapper.GpMapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Data
@WebFilter(filterName = "sessionFilter", urlPatterns = {"/*"})
public class LoginFilter implements Filter {

    private static List<String> includeUrls = new ArrayList<>();
    {
        includeUrls.add("/pgLogin.html");
        includeUrls.add("/index.html");
        includeUrls.add("/addMedStaff.html");
        includeUrls.add("/gp/logOut");
        includeUrls.add("/gp/login");
        includeUrls.add("/addMedStaff/insert");
    }
    @Autowired
    private GpMapper gpMapper;
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        String uri = request.getRequestURI();
        System.out.println("filter url:" + uri);
        boolean needFilter = isNeedFilter(uri);
        if (!needFilter) { //不需要过滤直接传给下一个过滤器
            filterChain.doFilter(servletRequest, servletResponse);
        } else { //需要过滤器
            // session中包含user对象,则是登录状态
            if (session != null && session.getAttribute("gp") != null) {
                Gp gp = (Gp)session.getAttribute("gp");
                Example example = new Example(Gp.class);
                example.createCriteria().andEqualTo("userName",gp.getUserName());
                List<Gp> gps = gpMapper.selectByExample(example);
                if(CollectionUtils.isEmpty(gps)){
                    response.sendRedirect(request.getContextPath() + "/index.html");
                }
                // System.out.println("user:"+session.getAttribute("user"));
                filterChain.doFilter(request, response);
            } else {
                //重定向到登录页(需要在static文件夹下建立此html文件)
                //response.sendRedirect(request.getContextPath() + "/index.html");
                response.sendRedirect( "index.html");
                return ;
            }
        }
    }

    /**
     * 过滤器初始化方法
     *
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // 保存不拦截的url
       /* String[] ignoreURLArray = ignoreURL.split(",");
        for (String url : ignoreURLArray) {
            passUrls.add(url.trim());
        }
        ctxPath = filterConfig.getServletContext().getContextPath();
        System.out.println("ctx = " + ctxPath);
        LOGGER.info("不拦截的URL包括:");
        for (String url : passUrls) {
            LOGGER.info(url);
        }*/
    }

    public boolean isNeedFilter(String uri) {
    	 System.out.println("this is a test");
        for (String includeUrl : includeUrls) {
            if (includeUrl.equals(uri)||uri.contains("jpg")) {
                return false;
            }
        }

        return true;
    }
}
