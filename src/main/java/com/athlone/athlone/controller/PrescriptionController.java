package com.athlone.athlone.controller;

import com.athlone.athlone.common.Result;
import com.athlone.athlone.entry.Gp;
import com.athlone.athlone.entry.Patient;
import com.athlone.athlone.entry.Prescription;
import com.athlone.athlone.entry.PrescriptionVO;
import com.athlone.athlone.mapper.PrescriptionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
@ResponseBody
@RequestMapping("prescription")
public class PrescriptionController {

    @Autowired
    private PrescriptionMapper prescriptionMapper;
    /**
     * add prescription
     * @param patientId pacientID
     * @param description  prescription
     * @param request
     * @return
     */




    @RequestMapping(value = "addPrescription")
    public Result addPrescription(@RequestParam Integer  patientId, @RequestParam String description, HttpServletRequest request ) {
        HttpSession session = request.getSession(false);
        Gp gp = (Gp)session.getAttribute("gp");
        Prescription prescription = new Prescription();
        prescription.setDescription(description);
        prescription.setPatientId(patientId);
        prescription.setAdvice("-");
        prescription.setCreateTime(new Date() );
        Integer gpId=gp.getId();
        prescription.setGpId(gpId);
        int count = prescriptionMapper.insertSelective(prescription);
        if(count>0){
            return  Result.successWithData(prescription);
        }
        return  Result.failWithMessage("add fail");

    }


    /**
     *  search prescription by id
     * @param id prescription ID
     * @return
     */
    @RequestMapping("show")
    public Result selectPrescription( @RequestParam Integer id) {
        Prescription prescription = prescriptionMapper.selectByPrimaryKey(id);
        if(prescription!=null){
            return Result.successWithData(prescription);
        }
        return Result.failWithMessage("no info");
    }

    @RequestMapping(value = "GPeditPrescription")
    public Result editPrescription( @RequestParam Integer id,@RequestParam String description ) {
        Prescription prescription = new Prescription();
        prescription.setId(id);
        prescription.setDescription(description);
        prescriptionMapper.updateByPrimaryKeySelective(prescription);
        return Result.successWithMessage("success");
    }
    @RequestMapping(value = "editPrescription")
    public Result editPrescription( @RequestParam Integer id,@RequestParam String description , @RequestParam String advice) {
        Prescription prescription = new Prescription();
        prescription.setId(id);
        prescription.setDescription(description);
        prescription.setAdvice(advice);
        prescriptionMapper.updateByPrimaryKeySelective(prescription);
        return Result.successWithMessage("success");
    }
    @RequestMapping(value = "PharmacistEditAdvice")
    public Result pharmacistEditAdvice( @RequestParam Integer id,@RequestParam String advice ) {
        Prescription prescription = new Prescription();
        prescription.setId(id);
        prescription.setAdvice(advice);
        prescriptionMapper.updateByPrimaryKeySelective(prescription);
        return Result.successWithMessage("success");
    }


    /**
     * search all info by id
     * @param patientId
     * @return
     */
    @RequestMapping(value = "summaryPrintout")
    public Result summaryPrintout( @RequestParam Integer patientId) {
        List<Prescription> list =prescriptionMapper.selectByPatientId(patientId);
        if(CollectionUtils.isEmpty(list)){
            return Result.failWithMessage("No prescription for the given patient");
        }
        return Result.successWithData(list);
    }

    @RequestMapping(value = "patientSelf",method = RequestMethod.GET)
    public Result patientSelf(HttpServletRequest request) {
        Patient patient=(Patient) request.getSession().getAttribute("patient");
        Integer patientId=patient.getId();
        List<Prescription> list =prescriptionMapper.selectByPatientId(patientId);
        if(CollectionUtils.isEmpty(list)){
            return Result.failWithMessage("No prescription for the given patient");
        }
        return Result.successWithData(list);
    }
}
