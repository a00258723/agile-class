package com.athlone.athlone.controller;

import com.athlone.athlone.common.Result;
import com.athlone.athlone.entry.Admin;
import com.athlone.athlone.entry.Gp;
import com.athlone.athlone.entry.Patient;
import com.athlone.athlone.mapper.PatientMapper;
import com.athlone.athlone.service.PatientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@RestController
@Slf4j
@RequestMapping(value = "patient")
public class PatientController {
    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private PatientService patientService;

    @RequestMapping(method = RequestMethod.GET)
    public Result selectAll() {
        List<Patient> patients = patientService.selectAll();
        if(patients!=null){
            return Result.successWithData(patients);
        }
        return Result.failWithMessage("no person");
    }

    /**
     * add patient
     * @param name
     * @param age
     * @param sex
     * @param mobile
     * @param allergies
     * @return
     */
  @RequestMapping(value = "add")
    public Result add(@RequestParam String userName, @RequestParam String passWord ,
                        @RequestParam String name, @RequestParam Integer age ,
                        @RequestParam Integer sex, @RequestParam String mobile,
                        @RequestParam String allergies,
                        @RequestParam String medicalConditions) {
        if(!patientService.checkIfExist(userName)) {
            Patient patient = new Patient();
            patient.setUserName(userName);
            patient.setPassWord(passWord);
            patient.setAge(age);
            patient.setAllergies(allergies);
            patient.setSex(sex);
            patient.setMobile(mobile);
            patient.setName(name);
            patient.setCreateTime(new Date());
            patient.setMedicalConditions(medicalConditions);
            patientService.insert(patient);
            return Result.successWithData(patient);
        }
        else return Result.failWithMessage("User already exists");
    }


    @RequestMapping(value = "update")
    public Result update(@RequestParam String userName, @RequestParam String name,
                         @RequestParam Integer age , @RequestParam Integer sex,
                         @RequestParam String mobile, @RequestParam String allergies,
                         @RequestParam String medicalConditions) {
        Patient patient = new Patient();
        patient.setUserName(userName);
        patient.setAge(age);
        patient.setAllergies(allergies);
        patient.setSex(sex);
        patient.setMobile(mobile);
        patient.setName(name);
        patient.setMedicalConditions(medicalConditions);

        patientService.update(patient);
        return Result.successWithData(patient);
    }

    /**
     * search pacient by Name
     * @param
     * @return
     */
    @RequestMapping(value = "selectByUserName")
    public Result selectByUserName(@RequestParam String   userName) {
        Patient patient = patientMapper.selectByUserName(userName);
        return Result.successWithData(patient);
    }

    @RequestMapping(value = "selectByUserNameWithPrescription")
    public Result selectByUserNameWithPrescription(@RequestParam String   userName) {
        List<Patient> patient = patientMapper.selectByUserNameWithPrescription(userName);
        return Result.successWithData(patient);
    }
    /**
     * search pacient by Id
     * @param
     * @return
     */
    @RequestMapping(value = "selectById")
    public Result selectById(@RequestParam Integer   id) {
        Patient patient = patientMapper.selectByPrimaryKey(id);
        if(patient!=null){
            return Result.successWithData(patient);
        }
        return Result.failWithMessage("no person");
    }

    @RequestMapping(value = "login" ,method = RequestMethod.POST)
    public Result login(@RequestParam String username, @RequestParam String password, HttpServletRequest request) {
        Patient patient= patientService.login(username.trim(),password.trim()) ;
        if (patient != null) {
            request.getSession().setAttribute("patient", patient);
            return Result.successWithData(patient);
        } else {
            return Result.failWithMessage("user not exists");
        }
    }


    @RequestMapping(value = "showLoginPatient",method = RequestMethod.GET)
    public Result showLoginPatient (HttpServletRequest request){
        Patient patient= (Patient) request.getSession().getAttribute("patient");
        if (patient !=null){
            return Result.successWithData(patient);
        }
        else {
            return Result.failWithMessage("no patient login");
        }
    }

    @RequestMapping(value = "logout" ,method = RequestMethod.POST)
    public Result logOut(HttpServletRequest request) {
        request.getSession().removeAttribute("patient");
        return Result.successWithMessage("success");
    }
}
