package com.athlone.athlone.controller;

import com.athlone.athlone.common.Result;
import com.athlone.athlone.entry.Admin;
import com.athlone.athlone.entry.Gp;
import com.athlone.athlone.mapper.GpMapper;
import com.athlone.athlone.service.AdminService;
import com.athlone.athlone.service.GpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
@RequestMapping(value = "gp")
public class GpLoginController {


    @Autowired
    private GpService gpService;

    @RequestMapping(value = "login")
    public Result login(@RequestParam String username, @RequestParam String password, HttpServletRequest request) {

        Gp gp = gpService.login(username.trim(), password.trim());
        if (gp != null) {
            request.getSession().setAttribute("gp", gp);
            return Result.successWithData("UserLogged in");
        } else {
            return Result.failWithMessage("user not exists");
        }
    }
  
    @RequestMapping(value = "insert")
    public Result insert(@RequestParam String username, @RequestParam String password, HttpServletRequest request) {
        if(!gpService.checkIfExist(username)) {
    		Gp gp = gpService.add(username, password);
        	if (gp!=null) {
        		return Result.successWithData(gp);
        	}
        	else return Result.failWithMessage("userCreationError");

    	}
    	else return Result.failWithMessage("User already exists");  			
    }

    @RequestMapping(value = "logout" ,method = RequestMethod.POST)
    public Result logOut(HttpServletRequest request) {
        request.getSession().removeAttribute("patient");
        return Result.successWithMessage("success");
    }
}
