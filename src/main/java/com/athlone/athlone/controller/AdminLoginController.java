package com.athlone.athlone.controller;

import com.athlone.athlone.common.Result;
import com.athlone.athlone.entry.Admin;
import com.athlone.athlone.mapper.AdminMapper;
import com.athlone.athlone.service.AdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
@RequestMapping(value = "admin")
public class AdminLoginController {


    @Autowired
    private AdminService adminService;

    @Autowired
    private AdminMapper adminMapper;

    @RequestMapping(value = "login")
    public Result login(@RequestParam String username, @RequestParam String password, HttpServletRequest request) {

        Admin admin= adminService.login(username.trim(),password.trim()) ;

        if (admin != null) {
            request.getSession().setAttribute("admin", admin);
            return Result.successWithData(admin);
        } else {
            return Result.failWithMessage("userDoesNotExist");
        }
    }

    @RequestMapping(value = "logout" ,method = RequestMethod.POST)
    public Result logOut(HttpServletRequest request) {
        request.getSession().removeAttribute("admin");
        return Result.successWithMessage("success");
    }
}
