package com.athlone.athlone.controller;

import com.athlone.athlone.common.Result;
import com.athlone.athlone.entry.Pharmacist;
import com.athlone.athlone.service.PharmacistService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
@RequestMapping(value = "pharmacist")
public class PharmacistController {

    @Autowired
    private PharmacistService pharmacistService;

    @RequestMapping(value ="insert" ,method = RequestMethod.POST)
    public Result insert(@RequestParam String username, @RequestParam String password, HttpServletRequest request) {
        Result res = new Result();
        if (pharmacistService.checkIfExist(username)!=null) {
            res.setMessage("User already in the system");
            res.setState(1);
        } else {
            pharmacistService.insert(username, password);
            if(pharmacistService.checkIfExist(username)!=null){
                res.setMessage("User successfully created !");
                res.setState(1);
            }
            else{
                res.setMessage("Something went wrong");
                res.setState(0);
            }
        }
        return res;
    }

    @RequestMapping(value = "login" ,method = RequestMethod.POST)
    public Result login(@RequestParam String username, @RequestParam String password, HttpServletRequest request) {
        Pharmacist pharmacist= pharmacistService.login(username.trim(),password.trim()) ;
        if (pharmacist != null) {
            request.getSession().setAttribute("patient", pharmacist);
            return Result.successWithData(pharmacist);
        } else {
            return Result.failWithMessage("user not exists");
        }
    }

    @RequestMapping(value = "logout" ,method = RequestMethod.POST)
    public Result logOut(HttpServletRequest request) {
        request.getSession().removeAttribute("patient");
        return Result.successWithMessage("success");
    }
}
