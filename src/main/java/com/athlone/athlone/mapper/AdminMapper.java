package com.athlone.athlone.mapper;

import com.athlone.athlone.entry.Admin;
import tk.mybatis.mapper.common.Mapper;

public interface AdminMapper extends Mapper<Admin> {
}