package com.athlone.athlone.mapper;

import com.athlone.athlone.entry.Gp;
import tk.mybatis.mapper.common.Mapper;

public interface GpMapper extends Mapper<Gp> {
}