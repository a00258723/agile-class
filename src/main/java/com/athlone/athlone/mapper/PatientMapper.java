package com.athlone.athlone.mapper;

import com.athlone.athlone.entry.Patient;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface PatientMapper extends Mapper<Patient> {
	@Select("SELECT * from teamw.patient  WHERE userName = #{userName}")
    Patient selectByUserName(String userName);

    @Select("select patient.*,prescription.advice from patient left  join prescription on patient.id=prescription.patient_id  WHERE patient.userName = #{userName}")
    List<Patient> selectByUserNameWithPrescription(String userName);

    @Select("SELECT * from teamw.patient")
    List<Patient> selectAllPatients();

    @Insert("INSERT INTO teamw.patient (userName, passWord,name, age,sex,mobile,allergies, createTime, medicalConditions) VALUES (#{userName}, #{passWord}, #{name},#{age},#{sex},#{mobile},#{allergies},now(),#{medicalConditions})")
    void insertPatient(Patient tmp);

    @Update("UPDATE teamw.patient SET name=#{name}, age = #{age} , sex=#{sex} , mobile = #{mobile} , allergies = #{allergies} , medicalConditions = #{medicalConditions}  WHERE userName=#{userName}")
    void  updatePatient(Patient tmp);


    @Select("select patient.*,prescription.advice from patient left  join prescription on patient.id=prescription.patient_id where patient.userName = #{arg0} and patient.passWord= #{arg1}")
    List<Patient> selectAlls(String username,  String password);
}