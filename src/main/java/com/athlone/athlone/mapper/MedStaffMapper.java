package com.athlone.athlone.mapper;

import com.athlone.athlone.entry.MedStaff;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

public interface MedStaffMapper extends Mapper<MedStaff> {

    @Select("SELECT id from teamw.medStaff  WHERE user_name = #{user_name}")
    Boolean selectMedStaff(String username);

    @Insert("INSERT INTO teamw.medStaff (user_name, pass_word, create_time) VALUES (#{userName}, #{passWord}, now())")
    void insertMedStaff(MedStaff medStaff);

}
