package com.athlone.athlone.mapper;

import com.athlone.athlone.entry.Prescription;
import com.athlone.athlone.entry.PrescriptionVO;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface PrescriptionMapper extends Mapper<Prescription> {

    @Select("SELECT * from teamw.prescription  WHERE patient_id = #{patientId}")
    List<Prescription> selectByPatientId(Integer patientId);
}