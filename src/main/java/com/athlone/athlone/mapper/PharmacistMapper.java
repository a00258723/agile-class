package com.athlone.athlone.mapper;

import com.athlone.athlone.entry.Pharmacist;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;


public interface PharmacistMapper extends Mapper<Pharmacist> {

    @Select("SELECT id from teamw.pharmacist  WHERE user_name = #{user_name}")
    Boolean selectPharmacist(String username);

    @Insert("INSERT INTO teamw.pharmacist (user_name, pass_word, create_time) VALUES (#{userName}, #{passWord}, now())")
    void insertPharmacist(Pharmacist pharmacist);
}
