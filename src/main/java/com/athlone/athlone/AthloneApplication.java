package com.athlone.athlone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import tk.mybatis.spring.annotation.MapperScan;


@Configuration
@ComponentScan
@MapperScan(basePackages = {"com.athlone.athlone.mapper"})
@SpringBootApplication
public class AthloneApplication {

    public static void main(String[] args) {
        SpringApplication.run(AthloneApplication.class, args);
    }
}
