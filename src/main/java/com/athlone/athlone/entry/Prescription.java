package com.athlone.athlone.entry;

import java.util.Date;
import javax.persistence.*;

@Table(name = "prescription")
public class Prescription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "gp_id")
    private Integer gp_id;

    @Column(name = "patient_id")
    private Integer patient_id;

    @Column(name = "description")
    private String description;

    @Column(name = "create_time")
    private Date create_time;

    @Column(name = "advice")
    private String advice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGpId() {
        return gp_id;
    }

    public void setGpId(Integer gp_id) {
        this.gp_id = gp_id;
    }

    public Integer getPatientId() {
        return patient_id;
    }

    public void setPatientId(Integer patient_id) {
        this.patient_id = patient_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return create_time;
    }

    public void setCreateTime(Date create_time) {
        this.create_time = create_time;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }
}