package com.athlone.athlone.entry;

import lombok.Data;

@Data
public class PrescriptionVO extends Prescription {
        private String  name;

        private Integer age;

        private Integer sex;

        private String mobile;

        private String allergies;

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public Integer getAge() {
                return age;
        }

        public void setAge(Integer age) {
                this.age = age;
        }

        public Integer getSex() {
                return sex;
        }

        public void setSex(Integer sex) {
                this.sex = sex;
        }

        public String getMobile() {
                return mobile;
        }

        public void setMobile(String mobile) {
                this.mobile = mobile;
        }

        public String getAllergies() {
                return allergies;
        }

        public void setAllergies(String allergies) {
                this.allergies = allergies;
        }
}
