Feature: GPadd

# Add hook to clear DB for every feature

Background:
# Create JDBC connection with DbUtils java class
* def config = { username: 'root', password: 'fjiksgef123dw', url: 'jdbc:mysql://api.devilez.eu:3308/teamw', driverClassName: 'com.mysql.jdbc.Driver' }
* def DbUtils = Java.type('com.athlone.athlone.TestUtils.DbUtils')
* def db = new DbUtils(config)


# calls a feature after every scenario to clear the database.
* configure afterScenario = 
"""
function(){
  var info = karate.info; 
  karate.log('after', info.scenarioType + ':', info.scenarioName);
  karate.call('after-scenario.feature', { caller: info.featureFileName });
}
"""

# parameter set base URL to set the corrext 'path' variable in the GIVEN statement below
# see 'path' uses url
# see karate-config.js which manages optional url names in a parameter in 'mvn verify -Pfailsafe -Dkarate.env=e2e' or no param means localhost     
* url baseUrl

Scenario: addGPSuccess  

Given path '/gp/insert'
And param username = 'Dave1'
And param password = 'Dave2'
When method GET
Then status 200
And match response contains {"success":true}

Scenario: GpAlreadyExists

# load a gp into DB
# Need to write query to add a specific admin u name and pwd
* def query = 'INSERT INTO gp (user_name,pass_word) VALUES ("DaveAlready","DaveAlready");'
* db.insertRows(query)

Given path '/gp/insert'
And param username = 'DaveAlready'
And param password = 'DaveAlready'
When method GET
Then status 200
And match response contains {"success":false,"message":"User already exists"}

    