package com.athlone.athlone.KarateIntegrationTests;

import com.intuit.karate.junit5.Karate;

class KarateTestsIT {

    @Karate.Test
    Karate testAll() {
        return Karate.run().relativeTo(getClass());
    }
}