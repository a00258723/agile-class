@ignore
Feature: Database cleaning n an 'afterScenario' hook


Scenario: clean the database before each scenario begins

* print 'in "DB_Cleaning after-scenario.feature", caller:', caller

#Create JDBC connection with DbUtils java class

* def config = { username: 'root', password: 'fjiksgef123dw', url: 'jdbc:mysql://api.devilez.eu:3308/teamw', driverClassName: 'com.mysql.jdbc.Driver' }
* def DbUtils = Java.type('com.athlone.athlone.TestUtils.DbUtils')
* def db = new DbUtils(config)


* db.cleanDatatable("DELETE from prescription;")
* db.cleanDatatable("DELETE from pharmacist;")
* db.cleanDatatable("DELETE from patient;")
* db.cleanDatatable("DELETE from medStaff;")
* db.cleanDatatable("DELETE from gp;")
* db.cleanDatatable("DELETE from admin;")


 
