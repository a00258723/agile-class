Feature: Patient

# Add hook to clear DB for every feature

Background:
# Create JDBC connection with DbUtils java class
* def config = { username: 'root', password: 'fjiksgef123dw', url: 'jdbc:mysql://api.devilez.eu:3308/teamw', driverClassName: 'com.mysql.jdbc.Driver' }
* def DbUtils = Java.type('com.athlone.athlone.TestUtils.DbUtils')
* def db = new DbUtils(config)


# calls a feature after every scenario to clear the database.
* configure afterScenario = 
"""
function(){
  var info = karate.info; 
  karate.log('after', info.scenarioType + ':', info.scenarioName);
  karate.call('after-scenario.feature', { caller: info.featureFileName });
}
"""
# use baseUrl as external parameter, local host or build host.     
* url baseUrl

Scenario: AddPatient

Given path '/patient/add'
And param userName = 'username'
And param passWord = 'password'
And param name = '222'
And param age = '222'
And param sex = '0'
And param mobile = '111' 
And param allergies = '1111'
And param medicalConditions = 'asthma'
When method GET
Then status 200
And match response contains {"success":true}



  Scenario: UpdatePatient
    Given path '/patient/update'
    And param userName = 'a'
    And param name = '222'
    And param age = '222'
    And param sex = '0'
    And param mobile = '222'
    And param allergies = '1111'
    And param medicalConditions = 'asthma'
    When method GET
    Then status 200
    And match response contains {"success":true}

  Scenario: PatientLoginSuccessfull


# load a admin into DB
# Need to write query to add a specific GP u name and pwd
    * def query = 'INSERT INTO patient (userName,passWord) VALUES ("patient","password");'
    * db.insertRows(query)


    Given path '/patient/login'
    And param username = 'patient'
    And param password = 'password'
    And request {}
    When method POST
    Then status 200
    And match response contains {"success":true}