Feature: GPLogin

# Add hook to clear DB for every feature

Background:
# Create JDBC connection with DbUtils java class
* def config = { username: 'root', password: 'fjiksgef123dw', url: 'jdbc:mysql://api.devilez.eu:3308/teamw', driverClassName: 'com.mysql.jdbc.Driver' }
* def DbUtils = Java.type('com.athlone.athlone.TestUtils.DbUtils')
* def db = new DbUtils(config)

# calls a feature after every scenario to clear the database.
* configure afterScenario = 
"""
function(){
  var info = karate.info; 
  karate.log('after', info.scenarioType + ':', info.scenarioName);
  karate.call('after-scenario.feature', { caller: info.featureFileName });
}
"""

# use baseUrl as external parameter, local host or build host.     
* url baseUrl

Scenario: NouserExists  

Given path '/gp/login'
And param username = 'Dave1'
And param password = 'Dave2'
When method GET
Then status 200
And match response == {"reason":null,"data":null,"success":false,"state":0,"message":"user not exists"}


Scenario: GpLoginSuccess  

# load a GP into DB
# Need to write query to add a specific GP u name and pwd
* def query = 'INSERT INTO gp (user_name,pass_word) VALUES ("DaveName","DavePwd");'
* db.insertRows(query)


Given path '/gp/login'
And param username = 'DaveName'
And param password = 'DavePwd'
When method GET
Then status 200
And match response contains {"success":true}


