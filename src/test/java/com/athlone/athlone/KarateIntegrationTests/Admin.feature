Feature: Admin

# Add hook to clear DB for every feature

Background:
# Create JDBC connection with DbUtils java class
* def config = { username: 'root', password: 'fjiksgef123dw', url: 'jdbc:mysql://api.devilez.eu:3308/teamw', driverClassName: 'com.mysql.jdbc.Driver' }
* def DbUtils = Java.type('com.athlone.athlone.TestUtils.DbUtils')
* def db = new DbUtils(config)


# calls a feature after every scenario to clear the database.
* configure afterScenario = 
"""
function(){
  var info = karate.info; 
  karate.log('after', info.scenarioType + ':', info.scenarioName);
  karate.call('after-scenario.feature', { caller: info.featureFileName });
}
"""

# use baseUrl as external parameter, local host or build host.     
* url baseUrl

Scenario: AdminLoginSuccessfull


# load a admin into DB
# Need to write query to add a specific GP u name and pwd
* def query = 'INSERT INTO admin (user_name,pass_word) VALUES ("admin","admin");'
* db.insertRows(query)

 
Given path '/admin/login'
And param username = 'admin'
And param password = 'admin'
When method GET
Then status 200
And match response contains {"success":true}

Scenario: AdminUserNameDoesNotExist

# GIVEN a valid pre existing admin user name and pwd,   
# WHEN admin login is initiated with a different username, 
# THEN login failure message stating incorrect username"


Given path '/admin/login'
And param username = 'admin'
And param password = 'admin'
When method GET
Then status 200
And match response == {"state":0,"data":null,"message":"userDoesNotExist","reason":null,"success":false}