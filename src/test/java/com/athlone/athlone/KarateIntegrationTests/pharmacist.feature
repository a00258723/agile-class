Feature: Patient

# Add hook to clear DB for every feature

  Background:
# Create JDBC connection with DbUtils java class
    * def config = { username: 'root', password: 'fjiksgef123dw', url: 'jdbc:mysql://api.devilez.eu:3308/teamw', driverClassName: 'com.mysql.jdbc.Driver' }
    * def DbUtils = Java.type('com.athlone.athlone.TestUtils.DbUtils')
    * def db = new DbUtils(config)


# calls a feature after every scenario to clear the database.
    * configure afterScenario =
"""
function(){
  var info = karate.info;
  karate.log('after', info.scenarioType + ':', info.scenarioName);
  karate.call('after-scenario.feature', { caller: info.featureFileName });
}
"""
# use baseUrl as external parameter, local host or build host.
    * url baseUrl

  Scenario: AddPharmacist

    Given path '/medstaff/insert'
    And param username = 'francois'
    And param password = 'password'
    And request {}
    When method POST
    Then status 200
    And match response contains {"success":true}

  Scenario: PharmacistLoginSuccessfull

# load a admin into DB
# Need to write query to add a specific GP u name and pwd
    * def query = 'INSERT INTO pharmacist (user_name,pass_word) VALUES ("pharmacist","password");'
    * db.insertRows(query)


    Given path '/pharmacist/login'
    And param username = 'pharmacist'
    And param password = 'password'
    And request {}
    When method POST
    Then status 200
    And match response contains {"success":true}