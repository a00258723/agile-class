function fn() {   
	karate.log('Med App karate.js called'); 
	var env = karate.env; // get system property 'karate.env'

	if (!env) {
		env = 'dev';
	}
	var config = {
			env: env,
	}
	if (env == 'dev') {
		// customize
		karate.log('Med App Karate Dev env'); 
		config.baseUrl = 'http://localhost:8081'
	} 
	else if (env == 'build') {
		// customize
		karate.log('Med App Karate Build env'); 
		config.baseUrl = 'http://api.devilez.eu:8081'
	}
	karate.log('Med App Karate returning URL'); 

	return config;
}