FROM openjdk:15.0.1
COPY ./target/athlone-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]